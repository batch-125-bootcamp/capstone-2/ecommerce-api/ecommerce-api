const Product = require("./../models/Product");
const User = require("./../models/User")

// create product as an admin

module.exports.createProduct = (reqBody) => {
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		return newProduct.save().then((res, err) => {
			if (err){
				return err
			} else {
				return res
			}
		})
		
	};

// retrieve all active products

module.exports.allActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	});
};

// retrieve all products
module.exports.allProducts = () => {
	return Product.find().then(result => {
		return result
	});
};

// retrieve single product

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	});
};


// update product info

module.exports.updateProduct = (userId, productId, reqBody) => {

	return User.findById(userId).then((result) => {
		if(result.isAdmin === true){
			let updatedProduct = {
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			}
			return Product.findByIdAndUpdate(productId, updatedProduct, {new: true}).then((res, err) => {
				if(err){
					return err
				} else {
					return res
				}
			})	
		} else {
			return false
		}

})};

// archive product

module.exports.archiveProduct = (productId) => {
	let archivedProduct = {
			isActive: false
		}
		return Product.findByIdAndUpdate(productId, archivedProduct, {new: true}).then((res, err) => {
			if(err){
				return err
			} else {
				return (`Product was archived successfully.`)
			}
		})
	
};

// unarchive product

module.exports.unarchiveProduct = (productId) => {
	
	let unarchivedProduct = {
		isActive: true
	}
	return Product.findByIdAndUpdate(productId, unarchivedProduct, {new: true}).then((res, err) => {
		if(err){
			return err
		} else {
			return (`Product was unarchived successfully.`)
		}
	})

};


// delete product

module.exports.deleteProduct = (productId) => {
	
	return Product.findByIdAndDelete(productId).then((res, err) => {
		if(err){
			return err
		} else {
			return (`Product was deleted successfully.`)
		}
	})

};
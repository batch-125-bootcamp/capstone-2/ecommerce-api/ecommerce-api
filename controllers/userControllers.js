const Products = require("./../models/Product");
const User = require("./../models/User");
const bcrypt = require("bcrypt");
const auth = require("./../auth");

// user registration

module.exports.register = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        userName: reqBody.userName,
        password: bcrypt.hashSync(reqBody.password, 5),

    });

    return newUser.save().then((res, err) => {
        if (err) {
            return err
        } else {
            return true
        }
    });

};

// check email for registration

module.exports.checkEmail = (reqBody) => {
    return User.find({ email: reqBody.email }).then((result) => {
        if (result.length != 0) {
            return true
        } else {
            return false
        }
    });

};

// login

module.exports.login = (reqBody) => {
    return User.findOne({email: reqBody.email }).then((result) => {
        if (result === null) {
            return false
        } else {
            const confirmPassword = bcrypt.compareSync(reqBody.password, result.password)

            if (confirmPassword === true) {
                return {access: auth.createAccessToken(result.toObject())}
            } else {
                return false
            }
        }
    });
};

// find all users

module.exports.getAllUsers = () => {
    return User.find().then((result) => {
        return result
    });
};

// find single user using userID

module.exports.getProfile = (data) => {
    return User.findById(data).then( result => {

        result.password = "******"
        return result
    })
}

// setting user as an admin

module.exports.setAdmin = (params) => {
    let updatedAdmin = {
        isAdmin: true
    }
    return User.findByIdAndUpdate(params, updatedAdmin, {new: true }).then((result, err) => {
        if (err) {
            return false
        } else
            return result
    });
};

// new order for non-admin

module.exports.newOrder = async (data) => {
    const userSaveStatus = await User.findById(data.userId).then(user => {
        if (user.isAdmin === false){
        let computedtotalAmount = parseInt(data.productPrice) * parseInt(data.quantity);
        user.order.push({
            products: {
                productId: data.productId,
                quantity: data.quantity
            },
            totalAmount: computedtotalAmount
        })
        return user.save().then((user, err) => {
            if(err){
                return false
            } else {
                return user
            }
        })
    }
})
    const productSaveStatus = await Products.findById(data.productId).then(product => {
        product.productOrdered.push({productId: data.productId})

        return product.save().then((product, err) => {
            if(err){
                return false
            } else {
                return product
            }
        })
    })

    if(userSaveStatus && productSaveStatus){
        return true
    } else {
        return false
    }
};

// retrieves my orders as nonadmin
module.exports.myOrders = (data) => {
    return User.findById(data.userId).then(user => {
        if(user.isAdmin === false){
        return user.order;
        }
    })
};


// retrieves all order as an admin
module.exports.allOrders = (data) => {
    return User.findById(data.userId).then(user => {
        if(user.isAdmin === true){
            return User.find({isAdmin: false}).then(result=> {

                let output = [{
                }];
                for (let i = 0; i < result.length; i++){
                    console.log(result[i].order)
                    output.push(result[i].order);
                }
                return output;
            })
        }
    })
};

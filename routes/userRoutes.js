const express = require("express");
const router = express.Router();
const userController = require("./../controllers/userControllers");
const auth = require("./../auth")

// register route
router.post("/register", (req, res) => {
	userController.register(req.body).then(result => res.send(result))
});

// check if username exists route
router.post("/emailExists", (req, res) => {
	userController.checkEmail(req.body).then(result => res.send(result))
});

// log in route
router.post("/login", (req, res) => {
	userController.login(req.body).then(result => res.send(result))
});

// get all users route
router.get("/allUsers", (req, res) => {
	userController.getAllUsers().then(result => res.send(result))
});

// get single user
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile(userData.id).then(result => res.send(result))
});

// set as admin route
router.put("/:userId/setAdmin", auth.verify, (req, res) => {
	userController.setAdmin(req.params.userId).then(result => res.send(result))
});

// add new order as non admin
router.post('/newOrder', auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity,
		productPrice: req.body.productPrice
	}

	userController.newOrder(data).then(result => res.send(result))
})


// retrieve all orders as an admin
router.get("/allOrders", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}
	userController.allOrders(data).then(result => res.send(result))
});

router.get("/myOrders", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id
	}
	userController.myOrders(data).then(result => res.send(result))
});



module.exports = router;
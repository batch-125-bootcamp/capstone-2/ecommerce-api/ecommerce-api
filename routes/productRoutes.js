const express = require("express");
const router = express.Router();
const productControllers = require("./../controllers/productControllers");
const auth = require("./../auth")

// create a product as an admin (not yet done as admin)
router.post("/createProduct", auth.verify, (req, res) => {
    productControllers.createProduct(req.body).then(result => res.send(result))
});

// retrieve all active products
router.get("/allActiveProducts", (req, res) => {
    productControllers.allActiveProducts().then(result => res.send(result))
});

// retrieve all products as an admin
router.get("/allProducts", (req, res) => {
    productControllers.allProducts().then(result => res.send(result))
});

// retrieve single products
router.get("/getProduct/:productId", (req, res) => {
    productControllers.getProduct(req.params).then(result => res.send(result))
});

// update product info as an admin
router.put("/updateProduct/:productId", auth.verify, (req, res) => {
    productControllers.updateProduct(req.params.productId, req.body).then(result => res.send(result))
});

// archive product as an admin 
router.put("/archiveProduct/:productId", (req, res) => {
    productControllers.archiveProduct(req.params.productId).then((result) => res.send(result))
});

// unarchive product as an admin 
router.put("/unarchiveProduct/:productId", (req, res) => {
    productControllers.unarchiveProduct(req.params.productId).then((result) => res.send(result))
});

// delete product as an admin 
router.delete("/deleteProduct/:productId", (req, res) => {
    productControllers.deleteProduct(req.params.productId).then((result) => res.send(result))
});

module.exports = router;
const mongoose = require("mongoose");
const userSchema = new mongoose.Schema(
	{
		firstName : {
			type: String,
			required: [true, "First Name is required."]
		},
		lastName: {
			type: String,
			required: [true, "Last Name is required."]
		},
		email: {
			type: String,
			required: [true, "Email is required."]
		},
		userName: {
			type: String,
			required: [true, "Username is required."]
		},
		password: {
			type: String,
			required: [true, "Password is required."]
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		order: [
			{
				products: [
					{
						productId: {
							type: String,
							required: [true, "Product Id is required."]
						},
						quantity: {
							type: Number,
							required: [true, "Product quantity is required."]
						}
						
					}
				],
				totalAmount: {
					type: Number,
					required: [true, "Total Amount is required."],
					default: 0
				},
				purchasedOn: {
					type: Date,
					default: new Date()
				}
			}
		]
	}

);

module.exports = mongoose.model("User", userSchema);
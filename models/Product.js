const mongoose = require("mongoose");
const productSchema = new mongoose.Schema(
	{
		name : {
			type: String,
			required: [true, "Name is required."]
		},
		description: {
			type: String,
			required: [true, "Description is required."]
		},
		price: {
			type: Number,
			required: [true, "Price is required."]
		},
		isActive: {
			type: Boolean,
			default: true
		}, 
		createdOn: {
			type: Date,
			default: new Date()			
		},
		productOrdered: [
			{
				productId: {
					type: String,
					required: [true, "Product Id is required."]
				},
				purchasedOn: {
					type: Date,
					default: new Date()
				},
				status: {
					type: String,
					default: "Checked Out"
				}
			}
		]
	}

);

module.exports = mongoose.model("Product", productSchema);